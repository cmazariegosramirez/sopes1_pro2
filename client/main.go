package main

import (
	"bufio"
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"strconv"
	"strings"
)

//Se almacenará un arreglo de casos de COVID-19
type Casos struct {
	Casos []caso `json:"Casos"`
}

//Struct para almacenar los casos de COVID-19
type caso struct {
	Nombre       string `json:"nombre"`
	Departamento string `json:"departamento"`
	Edad         int    `json:"edad"`
	Forma        string `json:"contagio"`
	Estado       string `json:"estado"`
}

func hiloEjec(canales chan string, index int, numDatos int, url string, ruta string, infoEnv int) {
	archivoJSON, err := os.Open(ruta)
	if err != nil {
		fmt.Println(err)
	}
	defer archivoJSON.Close()
	byteValue, _ := ioutil.ReadAll(archivoJSON)
	var listaCasos Casos
	json.Unmarshal(byteValue, &listaCasos)
	for i := index * infoEnv; i < index*infoEnv+infoEnv && i < numDatos; i++ {
		fmt.Println("index: ", index, " infoEnv: ", infoEnv, " i ", i)
		casoActual, _ := json.Marshal(listaCasos.Casos[i])
		respon, err := http.Post("http://"+url, "application/json", bytes.NewBuffer(casoActual))
		if err != nil {
			log.Println(err)
		}
		defer respon.Body.Close()
		body, err := ioutil.ReadAll(respon.Body)
		if err != nil {
			log.Println(err)
		}
		fmt.Println(string(body))
	}
	cadena := fmt.Sprintf("%s%d", "Hilo: ", index)
	canales <- cadena
}

func main() {
	urlDefault := "lbsopes1.ct.gt"
	fmt.Println("Ingrese URL a la que se enviarán las peticiones")
	fmt.Print("[" + urlDefault + "]: ")
	url, _ := bufio.NewReader(os.Stdin).ReadString('\n')
	url = strings.Trim(url, "\r\n ")
	if len(url) == 0 {
		url = urlDefault
	}

	_cwd, _ := os.Getwd()
	cwd := _cwd + "/data.json"
	fmt.Println("\nIngrese Path del archivo a leer")
	fmt.Print("[" + cwd + "]: ")
	path, _ := bufio.NewReader(os.Stdin).ReadString('\n')
	path = strings.Trim(path, "\r\n ")
	if len(path) == 0 {
		path = cwd
	}

	fmt.Println("\nIngrese # de Threads")
	fmt.Print("[1]: ")
	threadsIn, _ := bufio.NewReader(os.Stdin).ReadString('\n')
	threads, tErr := strconv.Atoi(strings.Trim(threadsIn, "\r\n "))
	if tErr != nil {
		threads = 1
	}

	fmt.Println("\n# de solicitudes del archivo")
	fmt.Print("[1]: ")
	noRequestFileIn, _ := bufio.NewReader(os.Stdin).ReadString('\n')
	noRequestFile, tErr := strconv.Atoi(strings.Trim(noRequestFileIn, "\r\n "))
	if tErr != nil {
		noRequestFile = 1
	}

	canales := make(chan string, threads)

	fmt.Println(url, path, threads, noRequestFile)
	for i := 0; i < threads; i++ {
		info := noRequestFile/threads + 1
		go hiloEjec(canales, i, noRequestFile, url, path, info)
	}
	contador := 0
	for element := range canales {
		if contador == threads-1 {
			close(canales)
		}
		contador++
		fmt.Println(element)
	}

}
