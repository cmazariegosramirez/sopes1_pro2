import React, { Component } from 'react';
import CanvasJSReact from '../../assets/canvasjs.react';
var CanvasJSChart = CanvasJSReact.CanvasJSChart;
 
class RAM1 extends Component {
  	constructor() {
		super();
	}
	
	
	render() {

		const datosDeptos = [{ y: 30, label: "Guatemala" },
		{ y: 30, label: "Izabal" },
		{ y: 40, label: "Peten" }

		]

		const datosEdades = [{ y:  1000, label: "20" },
		{ y:  3000, label: "30" },
		{ y:  10000, label: "40" },
		{ y:  30000, label: "50" },
		{ y:  100000, label: "60" },
		{ y:  200000, label: "70" },
		{ y:  300000, label: "80" }
	]

		const opcionesDeptos = {
			exportEnabled: true,
			animationEnabled: true,
			title: {
				text: "Deptos"
			},
			data: [{
				type: "pie",
				startAngle: 75,
				toolTipContent: "<b>{label}</b>: {y}%",
				showInLegend: "true",
				legendText: "{label}",
				indexLabelFontSize: 16,
				indexLabel: "{label} - {y}%",
				dataPoints: datosDeptos
			}]
		}

		const opcionesEdades = {
			animationEnabled: true,
			theme: "light2",
			title:{
				text: "Edades"
			},
			axisX: {
				title: "Cantidad",
				reversed: true,
			},
			axisY: {
				title: "Edades",
				labelFormatter: this.addSymbols
			},
			data: [{
				type: "bar",
				dataPoints: datosEdades
			}]
		}

		return (
		  <div>
			  	<h1>Coronavirus</h1>
				<h3>Departamentos</h3>
				<CanvasJSChart options = {opcionesDeptos} />
				<h3>Edades afectadas</h3>
				<CanvasJSChart options = {opcionesEdades} />
		  </div>
		);
	}
}
export default RAM1;
