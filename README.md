# Proyecto - Sistemas Operativos 1

Antes de correr estos scripts se debe de agregar las variables de entorno que estan en `.env`

Estas se agregan con
```bash
source k8s/.env
```

## 1. Instalar HELM
HELM es un menejador de "paquestes" para kubernetes. Esto nos ayudará a instalar un ingress de Nginx mas rápido y configurado al 100% en vez de crear por nosotros mismo toda la config YAML para k8s.

### Instalación
```bash
curl https://raw.githubusercontent.com/helm/helm/master/scripts/get-helm-3 | bash
```

Una vez innstalador podemos instalar NGNIX

```bash
helm install ingress-nginx/ingress-nginx --version 3.10.1 --generate-name --namespace $BACKEND_NS
```

## 2. Ejecutar `kubectl`
Aplicar los comandos para kubectl
```bash
kubectl apply -f k8s/
```

## 10. Agregar Cluster de GCP a mi `~/.kube/config` con `gcloud`
### Listar clusters (info)
```bash
gcloud container clusters list --project $GCP_PROJECT --zone $GCP_ZONE
```

### Obtener las credenciales del cluster para trabajar en local
Este comando agregará las credenciales automaticamente a `~/.kube/config` en mi PC para que pueda usar `kubectl`.
```bash
gcloud container clusters get-credentials $GCP_CUSTER_NAME --project $GCP_PROJECT --zone $GCP_ZONE
```
